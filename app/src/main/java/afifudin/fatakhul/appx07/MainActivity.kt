package afifudin.fatakhul.appx07

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var db : SQLiteDatabase
    lateinit var lsadapter: ListAdapter

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScanQR -> {
//                memberikan bunyi beep saat scan qr code
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGeneratQR -> {
                val barCodeEncoder =  BarcodeEncoder()
                val bitmap = barCodeEncoder.encodeBitmap(edQRCODE.text.toString(),
                    BarcodeFormat.QR_CODE,400,400)
                imgLoadQRCODE.setImageBitmap(bitmap)
                getTokenizer(edQRCODE.text.toString(),";")
            }R.id.btnSimpan -> {
                addMhs()
                Toast.makeText(this,"Data Berhasil disimpan",Toast.LENGTH_SHORT).show()
                showDataMhs()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intentResult != null){
            if(intentResult.contents != null){
                edQRCODE.setText(intentResult.contents)
                getTokenizer(edQRCODE.text.toString(),";")
            }else{
                Toast.makeText(this,"Dibatalkan",Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun getTokenizer(Kalimat : String, delimiter : String){
        val strToken = StringTokenizer(Kalimat,delimiter,false)
        txtNIM.setText(strToken.nextToken())
        txtNama.setText(strToken.nextToken())
        txtProdi.setText(strToken.nextToken())
    }



//    part of libary QR CODE
    lateinit var intentIntegrator: IntentIntegrator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        intentIntegrator = IntentIntegrator(this)
        btnGeneratQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)
        btnSimpan.setOnClickListener(this)
        getDbObject()

    }
    fun getDbObject() : SQLiteDatabase {
        db = DB(this).writableDatabase
        return db
    }
    fun addMhs() {
        var mhsNim = txtNIM.text.toString()
        var mhsNama = txtNama.text.toString()
        var mhsProdi = txtProdi.text.toString()
        var cv : ContentValues = ContentValues()
        cv.put("mhsNIM", mhsNim)
        cv.put("mhsNama",mhsNama)
        cv.put("mhsProdi", mhsProdi)
        db.insert("mhs",null,cv)

    }
    fun showDataMhs() {
        val c: Cursor = db.rawQuery("select mhsID as _id ,mhsNIM , mhsNama ,mhsProdi from mhs", null)
        lsadapter = SimpleCursorAdapter(
            this, R.layout.item_mahasiswa, c,arrayOf("mhsNIM", "mhsNama", "mhsProdi"), intArrayOf(R.id.nimMhs, R.id.namaMhs,R.id.prodiMhs),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        listMhs.adapter = lsadapter
    }

    override fun onStart() {
        super.onStart()
        showDataMhs()
    }

}
