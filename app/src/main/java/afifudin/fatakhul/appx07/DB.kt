package afifudin.fatakhul.appx07

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DB(context: Context):SQLiteOpenHelper(context,DB_Name,null,DB_ver) {

    companion object{
        val DB_Name = "mahasiswa"
        val DB_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val mhs = "create table mhs(mhsID integer primary key autoincrement, mhsNIM varchar(20) not null, mhsNama varchar(50) not null, mhsProdi varchar(20) not null)"
    db?.execSQL(mhs)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

}